import React from "react";
import "./Navbar.css"
import { FaShoppingCart } from "react-icons/fa";
import Cart from "../cart/Cart";
import { useCartContext,useProductsContext } from "../../javascript/context/CartContext";
import { useModalContext } from "../../javascript/context/UserContext";
import { useNavigate } from "react-router-dom";
import { CiLogout } from "react-icons/ci";
import { useState } from "react";
const Navbar = ()=>{
    const navigate = useNavigate()
    const [userModal,setUserModal] = useModalContext()
    const [products,setProducts] = useProductsContext()
    const [modal,setModal] = useCartContext()
    const [setting,setSetting] = useState(false)
    const quantity = products.reduce((acc,curr)=>{
        return acc + curr.quantity
    },0)
    const log = ()=>{
        navigate('/login')
    }
    const logout=()=>{
        localStorage.removeItem('user')
        return navigate('/login')
    }
    const user = JSON.parse(localStorage.getItem('user'))
    const userMenu = ()=>{
        return (
            <div className="user-menu">
                <div className="option user-profile" onClick={()=>setUserModal(!userModal)}><img className="user-img" src={user.image} alt=""  />{user.name}</div>
                <div className="option logut" onClick={()=>setModal(!modal)}><FaShoppingCart className="cart-icon"/><span>Carrito</span></div>
                <div className="option logut" onClick={logout}><CiLogout className="logout-icon"/><span>Cerrar sesion</span></div>
            </div>
        )
    }

    const settings = ()=>{
        setSetting(prev=>!prev)
    }
    return(
        <section id="navbar">
            <a className="cart-icon" onClick={()=>setModal(!modal)}><FaShoppingCart/>({quantity})</a>
            {user?null:<a onClick={()=>log()}>Iniciar</a>}
            {/* {user?userOps():null} */}
            {modal ? <Cart opacity={1} events={"auto"} />: <Cart opacity={0} events={"none"} />}
            {
                user?<img className="user-btn" src={user.image} alt="" onClick={settings} />:null
            }
            {
                setting?userMenu():null
            }
        </section>
    )
}
export default Navbar