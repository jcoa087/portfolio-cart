import React from "react";
import "./Product.css"
import { IoIosClose } from "react-icons/io";
import { BiPlus,BiMinus } from "react-icons/bi";

import { useProductsContext } from "../../../javascript/context/CartContext";
const Product = (props)=>{
    const [products,setProducts] = useProductsContext()

    const increase=()=>{
        let copy = [...products]
        const found = copy.find((item)=>item.id===props.item.id)
        if(found){
            found.quantity+=1
        }
        setProducts(copy) 

    }
    const decrease=()=>{
        let copy = [...products]
        const found = copy.find((item)=>item.id===props.item.id)
        if(found){
            found.quantity-=1
        }
        setProducts(copy)
    }
    const remove=()=>{
        if(products.length===1){
            localStorage.removeItem('cart')
            setProducts([])
        }else{
            const updatedProducts=products.filter(item=>item.id!==props.item.id)
            setProducts(updatedProducts)
        }
        
    }

    return(
        <article className="product">
            <div className="product-img">
                <a className="quit-product" onClick={()=>remove()} ><IoIosClose/></a>
            
                <div className="img" style={{backgroundImage:`url(${props.item.url})`}} ></div>
            </div>
            <div className="product-name">{props.item.name}</div>
            
            <div className="quantity">
                {props.item.quantity === 1 ? <div style={{pointerEvents:"none"}} className="minus" onClick={()=>decrease()} ><BiMinus/></div>:<div style={{pointerEvents:"auto"}} className="minus" onClick={()=>decrease()} ><BiMinus/></div>}
                <div className="quantity-total">{props.item.quantity}</div>
                <div className="plus" onClick={()=>increase()} ><BiPlus/></div>
            </div>
            <div className="product-price">$MXN{props.item.price}</div>
        </article>
    )
}

export default Product