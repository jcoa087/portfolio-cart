import React from "react";
import "./Cart.css"
import { IoIosClose } from "react-icons/io";
import Product from "./product/Product";
import { useProductsContext,useCartContext } from "../../javascript/context/CartContext";
const Cart =(props)=>{

    const [modal,setModal] = useCartContext()
    const [products,setProducts] = useProductsContext()
    const quantity = products.reduce((acc,item)=>{
        return acc + item.quantity
    },0)
    const total = products.reduce((acc,item)=>{
        return acc + (item.quantity * item.price)
        
    },0)
     return(
        <section className="cart" id="main-cart" style={{opacity:props.opacity,pointerEvents:props.events}}>
            <div className="cart-container">
                <div className="shopping-cart">
                    {products.length >0?<h3>Mi carrito</h3>:<h3>Tu carrito esta vacio.</h3>}
                    <div className="products">
                    {
                    products.map((product,idx)=>
                    <Product key={idx} item={product} />
                    )
                    }
                    </div>
                </div>
                <div className="summary">
                    <a className="close-cart" onClick={()=>setModal(!modal)}><IoIosClose/></a>
                    <h3>Resumen</h3>
                    <div className="items">
                        <label>Articulos</label>
                        <label>{quantity}</label>
                    </div>
                    <p>Envio</p>
                    <div className="total">
                        <label htmlFor="">total</label>
                        <label>$MXN{total}</label>
                    </div>
                    <button>Pagar</button>
                </div>
            </div>
        </section>
    )
}

export default Cart