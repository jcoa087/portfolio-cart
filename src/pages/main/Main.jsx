import React from 'react'
import Slider from './slider/Slider'
import Card from './card/Card'
import Hero from './hero/Hero'
import CardSlider from './card-slider/CardSlider'
import Navbar from '../navbar/Navbar'
const Main = () => {
  return (
    <main>
        <Navbar/>
        {/* <Hero/> */}
        {/* <Slider/> */}
        <Card/>
        {/* <Card/>
        <Card/>  */}
        {/* <CardSlider/> */}
    </main>
  )
}

export default Main