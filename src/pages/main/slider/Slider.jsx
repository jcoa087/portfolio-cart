import React, { useEffect, useState } from "react";
import './Slider.css'
import img from './../../../assets/hero.jpg'
import img2 from './../../../assets/jeans.jpg'
import img3 from './../../../assets/converse.jpg'
import img5 from './../../../assets/shirt.jpg'
import { AiOutlineRight,AiOutlineLeft } from "react-icons/ai";
import AOS from "aos";
import "aos/dist/aos.css";

const Slider = ()=>{
    useEffect(()=>{
        AOS.init({duration:1000});
        AOS.refresh();
    },[])
    const [slide,setSlide] = useState(0)
    const slides = [img,img2,img3,img5]
    const slide_right = ()=>{
        slide===slides.length-1 ?setSlide(0) : setSlide(slide+1)
    }
    const slide_left=()=>{
        const first = slide === 0
        first ? setSlide(slides.length-1) : setSlide(slide-1)
    }
    return(
        <section id="slider">
            <div className="overlay">
                <div data-aos="fade-left" className="see-btn">ver mas</div>
            </div>
            <section className="slide-container" style={{transform:`translateX(-${slide}00%)`}}>
                {
                    slides.map((slide,idx)=>
                    <img key={idx} src={slide} alt="" />
                    )
                }
                
            </section>
            <button className="right" onClick={()=>slide_right()}><AiOutlineRight className="right-icon"/></button>
            <button className="left" onClick={()=>slide_left()}><AiOutlineLeft className="left-icon"/></button>
        </section>
    )
}

export default Slider