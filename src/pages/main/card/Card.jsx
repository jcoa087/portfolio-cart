import React,{ useEffect, useState }  from "react";
import "./Card.css"
import img from "./../../../assets/sweater.jpg"
import img2  from "./../../../assets/tandem-x-visuals-cnkfZ8GujBg-unsplash.jpg"
import img3 from "./../../../assets/spencer-davis-_-pNeo5uDBs-unsplash.jpg"
import img4 from "./../../../assets/mohammed-hassan-5KAjSBSdaK4-unsplash.jpg"
import img5 from "./../../../assets/roman-holoschchuk-PoK8PDQVCME-unsplash.jpg"
import img6 from "./../../../assets/kaylee-garrett-M6obUe9T20o-unsplash.jpg"
import Modal from "./../../../utilities/modal/Modal.jsx"
import { useModalContext } from "../../../javascript/context/CustomContext";
import { useProductContext } from "../../../javascript/context/CartContext";
import AOS from "aos";
import "aos/dist/aos.css";
import { destroyItem } from "../../../javascript/api/api";
// import { useQuery } from "@tanstack/react-query";

const Card = ()=>{
    useEffect(()=>{
        AOS.init({duration:1000,once:true});
        AOS.refresh();
    },[])
    const cards = [ {id:1,name:"Lorem ipsum dolor sit.",price:200,size:"ch",color:"#333",url:img,quantity:1},
                    {id:2,name:"Lorem ipsum dolor sit.",price:250,size:"ch",color:"#333",url:img2,quantity:1},
                    {id:3,name:"Lorem ipsum dolor sit.",price:280,size:"ch",color:"#333",url:img3,quantity:1},
                    {id:4,name:"Lorem ipsum dolor sit.",price:300,size:"ch",color:"#333",url:img4,quantity:1},
                    {id:5,name:"Lorem ipsum dolor sit.",price:200,size:"ch",color:"#333",url:img5,quantity:1},
                    {id:6,name:"Lorem ipsum dolor sit.",price:210,size:"ch",color:"#333",url:img6,quantity:1}]
    const [product,setProduct]=useProductContext()
    const [modal,setModal] = useModalContext()

    const show_modal= (card)=>{
        setProduct({id:card.id,name:card.name,price:card.price,size:card.size,color:card.color,url:card.url,quantity:card.quantity})
        setModal(!modal)
    }    
    // const query = useQuery({queryKey:["items"],queryFn:()=>destroyItem()})
    // if(query.isFetching){return <h1>Obteniendo articulos</h1>}

    // if(query.isError){

    //  return <h1>Error obteniendo articulos</h1>
    // }
    // console.log(query.data)
    return(
        <section className="cards">
            <h2 data-aos="fade-right" >Invierno</h2>
            <div className="cards-container" data-aos="fade-left">
                {
                    cards.map((card,idx)=>
                    <a  key={idx} className="card" style={{backgroundImage:`url(${card.url})`}} onClick={()=>show_modal(card)}>
                        <div className="buy-btn">ver mas</div>
                    </a>
                    )
                }
            </div>
            {modal?<Modal  opacity={1} events={"auto"} />:<Modal  opacity={0} events={"none"} />}
        </section>
    )
}
export default Card