import React from 'react'
import "./CardSlider.css"
import img1 from "./../../../assets/card-slider/sneakers2.jpg"
import img2 from "./../../../assets/card-slider/joshua-rawson-harris-HUD9O_rJcn8-unsplash.jpg"
import img3 from "./../../../assets/card-slider/khalid-boutchich-xhPA_leYyUw-unsplash.jpg"
import img4 from "./../../../assets/card-slider/dmitriy-k-GJaUXfIIcwM-unsplash.jpg"
import img5 from "./../../../assets/card-slider/ruslan-fatihov-0_0mPwQXG2I-unsplash.jpg"
import img6 from "./../../../assets/card-slider/leonie-zettl-sVzZj9HO7xQ-unsplash.jpg"
import img7 from "./../../../assets/card-slider/alex-shaw-HWwA3fp4AaA-unsplash.jpg"
const CardSlider = () => {
    const array = [img1,img2,img3,img4,img5,img6,img7]
    return (
    <section className='card-slider'>
        {
            array.map((card,idx)=>
            <div className='card' key={idx} >
                <img src={`${card}`} alt="" />
            </div>
            )
        }
    </section>
    )
}

export default CardSlider