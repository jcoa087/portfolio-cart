import React from "react"
import Main from "./pages/main/Main"
import Login from "./layouts/login/Login"
import SignIn from "./layouts/signin/Signin"
import { UserContext } from "./javascript/context/UserContext"
import { CustomContext } from "./javascript/context/CustomContext"
import { CartContext } from "./javascript/context/CartContext"
import { Routes,Route } from "react-router-dom"
import AuthRoute from "./utilities/auth-route/AuthRoute"

function App() {


  return (
    <main>
      <UserContext>
        <CustomContext>
          <CartContext>
            <Routes>
              <Route path="/login" element={
                <AuthRoute>
                  <Login/>
                </AuthRoute>
              } />
              <Route path="/signin" element={
                <AuthRoute>
                  <SignIn/>
                </AuthRoute>
              }/>
              <Route path="/" element={<Main/>}/>
            </Routes>
          </CartContext>
        </CustomContext>
      </UserContext>
    </main>
  )
}

export default App
