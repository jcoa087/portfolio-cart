import React from "react";
import "./Modal.css"
import { AiFillHeart } from "react-icons/ai";
import { IoIosClose } from "react-icons/io";
import { useModalContext } from "../../javascript/context/CustomContext";
import { useProductContext,useProductsContext } from "../../javascript/context/CartContext";
import { addItem } from "../../javascript/api/api";
const Modal = (props)=>{
    const [product,setProduct] = useProductContext()
    const [products,setProducts] = useProductsContext()
    const [modal,setModal] = useModalContext()

    const addPro=()=>{
        let copy = [...products]   
        let found = copy.find((item)=>item.id===product.id)
        if(found){
            found.quantity+=1
        }else{
            copy.push(product)
        }
        setProducts(copy)
    }

    const save = ()=>{
        addItem('RTX',16900,'VENTUS','12GB','Black',1,1)

    }
    return(
        <article className="main-modal" id="modal" style={{opacity:props.opacity,pointerEvents:props.events}}>
            <div className="modal-container">
                <a  className="close" onClick={()=>setModal(!modal)}><IoIosClose/></a>
                <div className="modal-elements">
                    <img src={product.url} alt="" />
                    <div className="modal-description">
                        <h3>{product.name}</h3>
                        <p className="price">$MXN{product.price}</p>
                        <p className="all-sizes">talla</p>
                        <div className="sizes">
                            <div className="size">xs</div>
                            <div className="size">s</div>
                            <div className="size">m</div>
                            <div className="size">l</div>
                            <div className="size">xl</div>
                            <div className="size">xxl</div>
                        </div>
                        <p className="all-colors">color</p>
                        <div className="colors">
                            <div className="color" style={{backgroundColor:`#733528`}}></div>
                            <div className="color" style={{backgroundColor:`#3A221D`}}></div>
                            <div className="color" style={{backgroundColor:`#A20AA2`}}></div>
                            <div className="color" style={{backgroundColor:`#108553`}}></div>
                        </div>
                        <div className="shipping">
                            <div className="shipping-title">tipo de envio</div>
                            <div className="shipping-type">internacional</div>
                            <div className="shipping-description">Lorem ipsum dolor sit amet consectetur adipisicing elit. At aliquam reprehenderit exercitationem.</div>
                            
                        </div>
                        <div className="options">
                            <button onClick={addPro}>AÑADIR AL CARRITO</button>
                            <a href="#"><AiFillHeart/></a>
                        </div>
                    </div>
                </div>
            </div>
        </article>
    )
}

export default Modal