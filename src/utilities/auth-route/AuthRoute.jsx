import { Navigate } from "react-router-dom"

const AuthRoute = ({children}) => {
    const user = JSON.parse(localStorage.getItem('user'))
    return user?<Navigate to='/'/>:children

}

export default AuthRoute