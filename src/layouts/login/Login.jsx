import React from "react";
import "./Login.css"
import { useNavigate,Link } from "react-router-dom";
import { login } from "../../javascript/api/api";
import { useState } from "react";
import { validateExpression } from "../../javascript/validations/login-validations/login";
import { AiFillWarning } from "react-icons/ai";


const Login = () => {

  const [email,setEmail] = useState({status:'',message:''})
  const [password,setPassword] = useState({status:'',message:''})
  const [auth,setAuth] = useState(false)
  const [data,setData] = useState({
    email:'',
    password:''
  })
  
  const navigate = useNavigate()
  const log = ()=>{
    const loginObject = validateExpression(data.email,data.password)
    setAuth(false)

      login(data.email,data.password)

      setTimeout(() => {
        const user = JSON.parse(localStorage.getItem('user'))
        const error = JSON.parse(localStorage.getItem('error'))
        if(error!==null)
        {
          setAuth(true)
          return 
        }
        if(user.token && error===null){
          navigate('/')
        }

      }, 1500);

  }
  const logEnter=(e)=>{
    e.keyCode === 13?log():null
  }
  const handleInputChange = (event)=>{
    setData({
      ...data,
      [event.target.name] : event.target.value
    })
  }
  const authError = ()=>{
    return(
      <div className="auth-error">
        
        <div className="warning">
          <AiFillWarning/>
          <span>Ha ocurrido un error!</span>
        </div>
        <p>La cuenta ingresada no fue encontrada, intente de nuevo.</p>

      </div>
    )
  }
  return (
    <div className="login-container">
      <Link className="principal" to="/">Principal</Link>
      <div className="login">
        <div className="login-header">
          
          {auth?authError():<span>Bienvenido</span>}
        </div>
        <div className="login-body">
          <div className="body-content">
            <input className="input-field" placeholder="Correo electronico" type="email" name = "email" onChange={handleInputChange}/>
            <input className="input-field" placeholder="Contraseña" type="password" name = "password" onChange={handleInputChange} onKeyUp={logEnter}/>
            {
              data.email !=='' && data.password!==''?<div className="login-btn" onClick={log} >Iniciar</div>:null
            }
          </div>
        </div>
        <div className="login-footer">
          <Link to="/signin">¿No tienes cuenta?</Link>
          <a href="#">¿Olvidaste tu contraseña?</a>
        </div>
      </div>
    </div>
  )
}

export default Login