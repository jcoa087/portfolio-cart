import React, { useState } from 'react'
import "./Signin.css"
import { Link } from 'react-router-dom'
import { validateSignin } from '../../javascript/validations/signing-validations/signin'
import { signin } from '../../javascript/api/api'
import { useNavigate } from 'react-router-dom'
const SignIn = () => {
    const navigate = useNavigate()
    const [data,setData] = useState({
        name:"",
        lastname:"",
        email:"",
        password:"",
        confirm:"",
        image:""
    })
    const [validation,setValidation] = useState({
        error:false,
        name:{
            status:'',
            message:''
        },
        lastname:{
            status:'',
            message:''
        },
        email:{
            status:'',
            message:''
        },
        password:{
            status:'',
            message:''
        },
        confirm:{
            status:'',
            message:''
        }
    })
    const [auth,setAuth] = useState({
        message:'',
    })
    const sign = ()=>{
        const signinObject = validateSignin(data.name,data.lastname,data.email,data.password,data.confirm)
        setValidation({
            ...validation,
            error:signinObject['error'],
            name:{
                status:signinObject['name'].status,
                message:signinObject['name'].message
            },
            lastname:{
                status:signinObject['lastname'].status,
                message:signinObject['lastname'].message
            },
            email:{
                status:signinObject['email'].status,
                message:signinObject['email'].message
            },
            password:{
                status:signinObject['password'].status,
                message:signinObject['password'].message
            },
            confirm:{
                status:signinObject['confirm'].status,
                message:signinObject['confirm'].message
            },

        })
        if(signinObject['error']!==true){
            signin(data.name,data.lastname,data.email,data.password,data.image)
            setTimeout(() => {
                const user = JSON.parse(localStorage.getItem('user'))
                const error = JSON.parse(localStorage.getItem('error'))
    
                if(error!==null)
                {
                    if(error.status===409){
                        setAuth({...auth,message:'Escoge otro correo electrónico.',})
                    }
       
                    return 
                }
                if(user.token && error===null){
                  navigate('/')
                }
        
              }, 1500);
        }

    }
    const handelInputChange = (event)=>{
        setData({
            ...data,
            [event.target.name] : event.target.value 
        })   
    }
    const handleInputImage = (event) =>{
        setData({
            ...data,
            [event.target.name] : event.target.files[0] 
        })
    }
    const allowSave = ()=>{
        return(
            data.name !=='' && data.lastname !=='' && data.email !=='' && data.password!=='' && data.confirm !==''?<div className="signin-btn" onClick={sign}>Guardar</div>:null
        )
    }
  return (
    <div className='signin-container'>
        <Link className='principal' to="/">Principal</Link>
        <div className="signin">
            <div className="signin-header">
                <span>Crear cuenta</span>
            </div>
            <div className="signin-body">
                <div className="fullname">
                    <div className="input-form">
                    <input className={validation['name'].status==='error'?"input-error":'name'} type="text" name="name" placeholder="Nombre" onChange={handelInputChange}/>
                    {validation['name'].status==='error'?<span className="error-tooltip-name">{validation['name'].message}</span>:null}
                    </div>
                    <div className="input-form">
                    <input className={validation['lastname'].status==='error'?"input-error":null} type="text" name="lastname" placeholder="Apellido" onChange={handelInputChange}/>
                    {validation['lastname'].status==='error'?<span className="error-tooltip-lastname">{validation['lastname'].message}</span>:null}
                    </div>
                </div>
                <input className={validation['email'].status==='error'?"input-error":null}  type="email" name="email" placeholder="Correo electronico" onChange={handelInputChange}/>
                {validation['email'].status==='error'?<span className="error-message">{validation['email'].message}</span>:null}
                {auth['message']!==''?<span className="error-message">{auth['message']}</span>:null}
                <input className={validation['password'].status==='error'?"input-error":null}  type="text" name="password" placeholder="Contraseña" onChange={handelInputChange}/>
                {validation['password'].status==='error'?<span className="error-message">{validation['password'].message}</span>:null}
                <input className={validation['confirm'].status==='error'?"input-error":null}  type="text" name="confirm" placeholder="De nuevo tu contraseña" onChange={handelInputChange}/>
                {validation['confirm'].status==='error'?<span className="error-message">{validation['confirm'].message}</span>:null}
                <label htmlFor="files">Seleccionar imagen (opcional)</label>
                <input id='files' type="file" name="image" onChange={handleInputImage} />
                {allowSave()}
            </div>
            <div className="signin-footer">
                <Link to="/login">Iniciar</Link>
            </div>
        </div>
    </div>
  )
}

export default SignIn