import React, { createContext, useContext, useState } from "react";


const modalContext = createContext()

export const useModalContext=()=>{
    return useContext(modalContext)
}

export const CustomContext = ({children})=>{
    const [modal,setModal]=useState(false)

    return(
        <modalContext.Provider value={[modal,setModal]}>
                {children}
        </modalContext.Provider>
    )
}