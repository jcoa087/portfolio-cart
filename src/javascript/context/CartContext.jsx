import React, { createContext, useContext, useEffect, useState } from "react";


const cartContext = createContext()
const productCartContext = createContext()
const productsCartContext = createContext()


export const useCartContext=()=>{
    return useContext(cartContext)
}

export const useProductContext=()=>{
    return useContext(productCartContext)
}

export const useProductsContext=()=>{
    return useContext(productsCartContext)
}


export const CartContext = ({children})=>{
    const [modal,setModal]=useState(false)
    const [product,setProduct] = useState({id:0,name:"",price:0,size:"",color:"",url:""}) 
    const [cart,setCart] = useState(()=>{
        const getCart = JSON.parse(localStorage.getItem("cart")) ?? []
        return getCart
      })
  
    useEffect(()=>{
        if(cart.length>0)localStorage.setItem("cart",JSON.stringify(cart))
        setCart(cart)
    },[cart])

    return(
        <cartContext.Provider value={[modal,setModal]}>
                <productCartContext.Provider value={[product,setProduct]}>
                    <productsCartContext.Provider value={[cart,setCart]}>                   
                        {children}
                    </productsCartContext.Provider>
                </productCartContext.Provider>
        </cartContext.Provider>
    )
}