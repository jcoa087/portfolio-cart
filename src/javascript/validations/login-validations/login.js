import { validate } from "../validate.js"
export const EMAIL_EXPPRESSION = '^[a-zA-Z0-9._-Ññ]{3,}@[[a-zA-Z0-9._-]+[.][a-zA-Z]{2,4}$'
export const PASSWORD_EXPRESSION = '^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,}$'
export const MESSAGES =
{
    status:'error',
    email:
    {
        format:'Revisa tu correo electrónico.',
        unauthorized:'Contraseña y/o correo invalido(s)'
    },
    password:
    {
        format:'Tu contraseña debe tener al menos 8 caracteres. Al menos una minúscula, al menos una mayúscula y al menos un número.',
        unauthorized:'Contraseña y/o correo invalido(s)' 
    }
}


export const validateExpression = (email,password)=>{
    let loginObject = {
        email:{status:'',message:''},
        password:{status:'',message:''}
    }
    const validatedEmail = validate(email,EMAIL_EXPPRESSION)
    const validatedPassword = validate(password,PASSWORD_EXPRESSION)

    if(validatedEmail===null){
        loginObject['email']['status'] = MESSAGES['status']
        loginObject['email']['message'] = MESSAGES['email']['format']
    }
    if(validatedPassword===null){
        loginObject['password']['status'] = MESSAGES['status']
        loginObject['password']['message'] = MESSAGES['password']['format']
    }

    return loginObject
}



