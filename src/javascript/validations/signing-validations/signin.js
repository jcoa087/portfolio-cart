import { validate } from "../validate.js"
export const NAME_EXPRESSION = '^[a-zA-ZñÑ]{3,}$'
export const EMAIL_EXPPRESSION = '^[a-zA-Z0-9._-Ññ]{3,}@[[a-zA-Z0-9._-]+[.][a-zA-Z]{2,4}$'
export const PASSWORD_EXPRESSION = '(?=.*[A-Z])(?=.*\\d)(?=.*[@$!%*?&\.])[A-Za-z\\d@$!%*?&\.]{8,}'
export const MESSAGES =
{
    status:'error',
    name:{
        format:'Tu nombre debe tener al menos 3 letras. No se permiten simbolos y/o espacios.',
    },
    lastname:{
        format:'Tu apellido debe tener al menos 3 letras. No se permiten simbolos y/o espacios.',
    },
    email:
    {
        format:'Revisa tu correo electrónico.',
    },
    password:
    {
        format:'Tu contraseña debe tener al menos 8 caracteres. Una mayuscula. Una minuscula. Un caracter especial (@$!%*?&)',
    },
    confirm:{
        format:'Tus contraseñas deben coincidir.'
    }
}


export const validateSignin = (name,lastname,email,password,confirm)=>{
    let signinObject = {
        error:false,
        name:{status:'',message:''},
        lastname:{status:'',message:''},
        email:{status:'',message:''},
        password:{status:'',message:''},
        confirm:{status:'',message:''}
    }
    const validatedName = validate(name,NAME_EXPRESSION)
    const validatedLastName = validate(lastname,NAME_EXPRESSION)
    const validatedEmail = validate(email,EMAIL_EXPPRESSION)
    const validatedPassword = validate(password,PASSWORD_EXPRESSION)
    const validatedConfirm = password===confirm?'':null

    if(validatedName===null){
        signinObject['error']=true
        signinObject['name']['status'] = MESSAGES['status']
        signinObject['name']['message'] = MESSAGES['name']['format']
    }

    if(validatedLastName===null){
        signinObject['error']=true
        signinObject['lastname']['status'] = MESSAGES['status']
        signinObject['lastname']['message'] = MESSAGES['lastname']['format']
    }

    if(validatedEmail===null){
        signinObject['error']=true
        signinObject['email']['status'] = MESSAGES['status']
        signinObject['email']['message'] = MESSAGES['email']['format']
    }

    if(validatedPassword===null){
        signinObject['error']=true
        signinObject['password']['status'] = MESSAGES['status']
        signinObject['password']['message'] = MESSAGES['password']['format']
    }

    if(validatedConfirm===null){
        signinObject['error']=true
        signinObject['confirm']['status'] = MESSAGES['status']
        signinObject['confirm']['message'] = MESSAGES['confirm']['format']
    }

    return signinObject
}

