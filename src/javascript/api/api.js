import axios from "axios";

const BASE_URL = "https://api-cart.xyz/";

const user = JSON.parse(localStorage.getItem('user'))

export const signin = async(name,lastname,email,password,image)=> await axios({
    method:'post',
    url:`${BASE_URL}/user`,
    headers:{'Content-Type':'multipart/form-data'},
    data:{
        name:name,
        lastname:lastname,
        email:email,
        password:password,
        image:image
    }
})
.then((res)=>{
    localStorage.clear()
    console.log(res)
    if(res.status===200){
        const user = 
        {
            name:res.data.user[0].name,
            image:res.data.user[0].image,
            token:res.data.token
        }
        localStorage.setItem('user',JSON.stringify(user))
    }

})
.catch((error)=>{
    localStorage.clear()
    console.log(error)
    if(error.response.status!==200){
        localStorage.setItem('error',JSON.stringify(
            {status:error.response.status,statusText:error.response.statusText}
        ))
    }
    
})
export const login = async(email,password)=>await axios({
    method:'post',
    url:`${BASE_URL}/login`,
    headers:{'Content-Type':'multipart/form-data'},
    data:{
        email:email,
        password:password
    }
})
.then((res)=>{
    localStorage.clear()

    if(res.status===200){
        const user = 
        {
            name:res.data.user[0].name,
            image:res.data.user[0].image,
            token:res.data.token
        }
        localStorage.setItem('user',JSON.stringify(user))
    }

})
.catch((error)=>{
    localStorage.clear()
    if(error.response.status!==200)localStorage.setItem('error',JSON.stringify(
        {status:error.response.status,statusText:error.response.statusText}
    ))
    
})

export const destroyItem = async() => await axios
.delete(`${BASE_URL}/item/destroy`)
.then((res)=>{
    return res.data
})

export const addItem = async(name,price,model,ram_size,color,brand_id,provider_id) => await
axios({
    method:'post',
    url:`${BASE_URL}/item`,
    headers:{
        'Content-Type': 'multipart/form-data',
        'Authorization':`Bearer ${user.token}`,
    },
    data:{
        name:name,
        price:price,
        model:model,
        ram_size:ram_size,
        color:color,
        brand_id:brand_id,
        provider_id:provider_id
    }
})
.then((res)=>{
   console.log(res)
})
.catch(function(error) {
    console.log(error);
});